#!/bin/sh

echo "DestroyMenu RootMenu"
echo "AddToMenu RootMenu"

PATH=$PATH:/usr/games/

# No test for xterm -- the system better have that.
echo "+ \"%mini.term.xpm%xterm\" Exec exec $XTERM -geom 80x24"

if [ `which xmms` ]; then
	echo "+ \"%mini.sound.xpm%xmms\" Exec exec xmms > /dev/null 2>&1"
fi

echo "+ \"\" Nop"

if [ `which opera` ]; then
	echo "+ \"%mini.window2.xpm%Opera\" Exec exec opera > /dev/null 2>&1"
fi

if [ `which mozilla` ]; then
	echo "+ \"%mozilla.mini.xpm%Mozilla\" Exec exec mozilla >/dev/null 2>&1"
fi

if [ `which mozilla-firefox` ]; then
	echo "+ \"%mozilla.mini.xpm%Mozilla Firefox\" Exec exec mozilla-firefox >/dev/null 2>&1"
fi

if [ `which firefox` ]; then
	echo "+ \"%mozilla.mini.xpm%Mozilla Firefox\" Exec exec firefox >/dev/null 2>&1"
fi

if [ -x $HOME/packages/mozilla/mozilla ]; then
	echo "+ \"%mozilla.mini.xpm%Mozilla (local)\" Exec exec $HOME/packages/mozilla/mozilla"
fi

if [ -x $HOME/packages/firefox/firefox ]; then
	echo "+ \"%mozilla.mini.xpm%Mozilla Firefox (local)\" Exec exec $HOME/packages/firefox/firefox"
fi

if [ `which netscape` ]; then
	echo "+ \"%mini.nscape.xpm%Netscape\" Exec exec netscape"
fi

if [ `which xdemineur` ]; then
	echo "+ \"%mini.zoom.xpm%Xdemineur\" Exec exec xdemineur"
fi

echo "+ \"\" Nop"

if [ `which tik` ]; then
	echo "+ \"%mini.blah.xpm%TiK\" Exec exec tik >/dev/null 2>&1"
fi

if [ `which gaim` ]; then
	echo "+ \"%mini.gnome.xpm%gaim\" Exec exec gaim"
fi

if [ `which gimp` ]; then
	echo "+ \"%mini.gimp.xpm%GIMP\" Exec exec gimp"
fi

if [ `which gvim` ]; then
	echo "+ \"%mini.window.xpm%ViM\" Exec exec gvim -geom 80x24"
fi

echo "+ \"\" Nop"

echo "+ \"%mini.windows.xpm%WindowOps\" Popup WindowOps"
echo "+ \"%mini.windows.xpm%Modules\" Popup ModulesMenu"

if [ -f /etc/X11/fvwm/menudefs.hook ]; then
	echo "Read /etc/X11/fvwm/menudefs.hook"
	# This adds to other menus.  We must switch back.
	echo "AddToMenu RootMenu"
	echo "+ \"%mini.windows.xpm%Debian\" Popup \"/Debian\""
fi

echo "+ \"%mini.hdisk.xpm%Regenerate\" PipeRead ${FVWM_USERDIR}/menugen.sh"

if [ `which xlock` ]; then
	echo "+ \"\" Nop"
	echo "+ \"%mini.lock.xpm%xlock\" Exec exec xlock"
fi
