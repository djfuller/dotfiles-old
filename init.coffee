# Your init script
#
# Atom will evaluate this file each time a new window is opened. It is run
# after packages are loaded/activated and after the previous editor state
# has been restored.
#
# An example hack to log to the console when each text editor is saved.
#
# atom.workspace.observeTextEditors (editor) ->
#   editor.onDidSave ->
#     console.log "Saved! #{editor.getPath()}"



# apm install vim-mode-plus ex-mode termination

atom.packages.onDidActivatePackage (pkg) ->
  switch pkg.name
    when 'termination'
      # Utterly shamelessly hack term.js's event listner so we can trap
      # additional keystrokes.
      Terminal = require pkg.path + '/node_modules/term.js/src/term.js'
      kd = Terminal.prototype.keyDown
      Terminal.prototype.keyDown = (event) ->
        if event.ctrlKey and event.key == 'Tab'
          atom.commands.dispatch event.target, 'termination:focus'
        else
          kd.bind(this) event

      atom.commands.add 'atom-text-editor',
        'user:addOrFocusTermination': (event) ->
          if document.getElementsByClassName("termination terminal-view").length
            cmd = 'termination:focus'
          else
            cmd = 'termination:toggle'
          atom.commands.dispatch event.target, cmd

    when 'ex-mode'
      Ex = pkg.mainModule.provideEx()
      Ex.registerAlias 'vi', 'edit'
