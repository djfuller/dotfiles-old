(when (< emacs-major-version 24) (error "This version of emacs is too old."))

(setq df-window (getenv "WINDOW"))
(when df-window
  (defun mode-line-frame-control ()
    (format "W%-3d " (string-to-number df-window))))

(unless (window-system) (menu-bar-mode -1))
(show-paren-mode t)
(setq scroll-step 1)
(setq scroll-conservatively 50)
(setq make-backup-files nil)
(setq evil-want-C-u-scroll t)
(load-theme 'tango-dark)
(set-background-color "black")
(custom-theme-set-faces 'tango-dark '(default ((t (:background "#000000")))))

(setq-default c-default-style "linux"
	      c-basic-offset 8
	      c-tab-always-indent 'complete
	      tab-always-indent 'complete)

(c-set-offset 'innamespace 0)

(defun df-fix-underscore () (modify-syntax-entry ?_ "w"))
(defvar df-fix-underscore-hooks '(c-mode-hook c++-mode-hook))
(dolist (h df-fix-underscore-hooks)
  (add-hook h 'df-fix-underscore))

(unless (assoc "\\.yaml\\'" auto-mode-alist)
  (push '("\\.yaml\\'" . conf-mode) auto-mode-alist))

(defvar df-packages '(evil))
(require 'package)
(push '("melpa" . "http://melpa.milkbox.net/packages/") package-archives)
(package-initialize)
(unless package-archive-contents (package-refresh-contents))
(dolist (p df-packages)
  (when (not (package-installed-p p))
    (package-install p)))

(require 'evil)
(evil-mode 1)

(defvar busted-modes '(info-mode package-menu-mode))
(dolist (m busted-modes)
  (evil-set-initial-state m 'normal))

(defun other-file-and-search ()
  (interactive)
  (let ((sym (thing-at-point 'symbol)))
    (ff-find-other-file)
    (when sym
      ; evil-goto-definition looks at point for the symbol. First we look for
      ; the symbol and propagate an error if not found.
      (condition-case nil
	(evil-search (format "\\_<%s\\_>" (regexp-quote sym)) t t (point-min))
	(user-error (user-error "No match")))
      (evil-goto-definition))))

(evil-ex-define-cmd "vi" 'evil-edit)
(evil-ex-define-cmd "ta" 'find-tag)

(define-key global-map (kbd "RET") 'newline-and-indent)
(evil-define-key 'normal dired-mode-map
  "n" 'evil-search-next
  "G" 'evil-goto-line)
(evil-define-key 'insert comint-mode-map (kbd "C-w C-w") 'evil-window-next)
(evil-define-key 'normal comint-mode-map (kbd "C-w C-w") 'evil-window-next)
(define-key evil-normal-state-map (kbd "g e") 'next-error)
(define-key evil-normal-state-map (kbd "g h") 'other-file-and-search)
(define-key evil-normal-state-map (kbd "g l") 'buffer-menu)
(define-key evil-normal-state-map (kbd "C-w 1") 'delete-other-windows)
(define-key evil-normal-state-map (kbd "Q") 'evil-record-macro)
(define-key evil-normal-state-map (kbd "C-w SPC") 'scroll-other-window)

(define-key evil-motion-state-map (kbd "TAB") 'indent-region)

(defun df-backward-kill-line ()
  (interactive)
    (kill-line 0)
    (indent-according-to-mode))
(define-key evil-insert-state-map (kbd "C-u") 'df-backward-kill-line)

(defun quit-other-window ()
  (interactive)
  (unless (eq (next-window) (selected-window))
    (quit-window nil (next-window))))
(define-key evil-normal-state-map (kbd "q") 'quit-other-window)

(when (package-installed-p 'ggtags)
  (defun enable-ggtags ()
    (ggtags-mode)
    (define-key evil-normal-state-map (kbd "C-]") 'ggtags-find-tag-dwim)
    (define-key evil-normal-state-map (kbd "C-o") 'ggtags-prev-mark))
  (add-hook 'c++-mode-hook 'enable-ggtags))

(defun df-tab-insert ()
  (interactive)
  (if (looking-at "\\_>")
      (evil-complete-next)
    (c-indent-line-or-region)))
(define-key evil-insert-state-map (kbd "TAB") 'df-tab-insert)

(defvar df-extra-c-keywords '("EPERM" "ENOENT" "EIO" "ENXIO" "EAGAIN"
			      "ENOMEM" "EACCES" "EBUSY" "EINVAL" "ENOSPC"
			      "EIO" "ERANGE"))
			      
(font-lock-add-keywords 'c-mode
			(mapcar
			 (lambda (x) (cons x 'font-lock-variable-name-face))
			 df-extra-c-keywords))

(when (package-installed-p 'magit)
  (defvar df-previous-line nil)
  (defun df-goto-previous-line ()
    (interactive)
    (when df-previous-line
      (goto-char (point-min))
      (search-forward df-previous-line)
      (previous-line)
      (setq df-previous-line nil)))

  (add-hook 'magit-post-refresh-hook 'df-goto-previous-line)

  (defun rebase-warp ()
    (interactive)
    (setq df-previous-line (thing-at-point 'line))
    (magit-blame-copy-hash)
    (magit-blame-quit)
    (magit-rebase-edit-commit (car kill-ring) nil))

  (defun rebase-complete ()
    (interactive)
    (setq df-previous-line (thing-at-point 'line))
    (magit-stage-file (buffer-file-name))
    (magit-commit-amend))

  (defun rebase-continue ()
    (interactive)
    (setq df-previous-line (thing-at-point 'line))
    (magit-rebase-continue))

  (define-key evil-normal-state-map (kbd "g r") 'rebase-warp)
  (define-key evil-normal-state-map (kbd "g c") 'rebase-continue))
