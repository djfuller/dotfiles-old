setopt noautolist
setopt noautomenu
setopt c_bases
setopt correct
setopt csh_junkie_history
setopt ksh_option_print
setopt printexitvalue

ulimit -c unlimited

#if [[ $ZSH_VERSION > 4.0.4 ]] then
#	setopt auto_continue
#fi

if [[ -v TMUX_PANE ]] then
	WINDOW=$(tmux display-message -p '#I')
fi

prompt="%m${WINDOW+:$WINDOW}%(!.#.>) "

path=($path /usr/sbin /sbin $HOME/bin)
LISTMAX=10000
HISTSIZE=10000

export CVS_RSH=ssh
export TEXINPUTS=.:$HOME/.latex:
export BIBINPUTS=$TEXINPUTS
export EDITOR=vi

unalias ls which >& /dev/null
alias more=less

bindkey -d
bindkey -v
bindkey '^k' _complete_help  # autoloaded via compinit below
bindkey '\e[A' vi-up-line-or-history-that-does-not-suck  # defined below
bindkey '\eOA' vi-up-line-or-history-that-does-not-suck  # defined below
bindkey -a '\e[A' vi-up-line-or-history
bindkey -a '\eOA' vi-up-line-or-history
bindkey -a '\e[B' vi-down-line-or-history
bindkey -a '\eOB' vi-down-line-or-history
bindkey -a \? vi-history-search-backward

case $OSTYPE in
	darwin*)
		top () {
			command top -ocpu
		}
	;;
esac

if [[ ! -z $VTE_VERSION ]]
then
	open () {
		if [ -d ${1} ]
		then
			nautilus ${1} &
		else
			gnome-open ${1}
		fi
	}
fi

autoload -U compinit
compinit -u
zstyle -d

zstyle ':completion:*' users-hosts \
  dfuller@djfuller.ddns.net \
  dfuller@rex001.front.lab.lax.redhat.com

zstyle ':completion:*' group-name ''
zstyle ':completion:*:(c++|g++):*' file-patterns '*.(c|C|cc|cpp|cxx|o|y)'

compdef _gcc cc  # XXX: assumes cc == gcc
compdef _gcc c++

h () { mosh -ssh "ssh -p 2223" dfuller@djfuller.ddns.net }
setenv () { echo "you dolt."; export $1=$2 }
unsetenv () { echo "you dolt."; unset $1 }

vi-up-line-or-history-that-does-not-suck ()
{
	zle vi-up-line-or-history;
	zle vi-cmd-mode;
}

zle -N vi-up-line-or-history-that-does-not-suck \
       vi-up-line-or-history-that-does-not-suck

cal ()
{
	if [[ $# -eq 1 && $1 -le 12 ]] then
		command cal $1 $(date +%Y)
	else
		command cal $*
	fi
}

hatprompt() { export PS1="$(tput setaf 1)🎩︎$(tput sgr0)$PS1" }

title () { echo "\033]0;$1\007" }

pc () { print "git clone https://djfuller@bitbucket.com/djfuller/dotfiles" }

findch () { find . -type f -name \*\[ch\] -exec grep $* {} \; -print }

git ()
{
	hyphen_arguments=("${(@M)@:#-*}")
	if [[ $1 == "branch" && $hyphen_arguments == "" ]] then
		echo "don't do it that way"
		return 1
	else
		command git $*
	fi
}

stats ()
{
	awk \
		'BEGIN { sum=0; sumsq=0; } \
		{ \
			if (min == "") min=$1; if ($1 < min) min=$1; \
			if (max == "") max=$1; if ($1 > max) max=$1; \
			sum+=$1; sumsq+=$1*$1; \
		} \
		END { \
			printf "n: %d, sum: %f\nmin: %f, max: %f, mean: %f, stddev: %f\n", \
				NR, sum, min, max, sum/NR, sqrt(sumsq/NR - (sum/NR)**2) \
		}'
}

srcgrep ()
{
	find . -type f \( -name \*.c -o -name \*.cc -o -name \*.cpp -o -name \*.h -o -name \*.hpp \) -exec grep -H "${1}" {} \;
}

cdlast () { cd *(om[1]) }

errno ()
{
	headers=("/usr/include/asm-generic/errno-base.h"
		"/usr/include/asm-generic/errno.h")
	grep "${1}" $headers
}

bwnote ()
{
	if [[ ! (-n "${commands[bw]}") ]]
	then
		echo "Bitwarden not found"
		return
	fi
	if [[ ! (-n "${commands[jq]}") ]]
	then
		echo "jq not found"
		return
	fi

	if [[ ! (-v BW_SESSION) ]]
	then
		export BW_SESSION=$(bw unlock --raw)
	fi

	note=$(bw get item ${1})
	if [[ $note == "Not found." ]]
	then
		echo ${1} not found
		return
	fi
	if [[ $note == "Vault is locked." ]]
	then
		unset BW_SESSION
		echo "Incorrect password"
		return
	fi

	echo $(echo $note | jq -r .notes)
}


if [[ -d ~/.zsh ]] then
	files=(${=$(print ~/.zsh/*)})
	for i in $files; do
		if [[ $HOST == ${~i:t:r} ]] then
			. $i
		fi
	done
fi
