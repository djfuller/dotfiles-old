#!/bin/sh

#.tikrc/.fvwm2rc are handled as special cases below.
repo="$HOME/dotfiles"
update=1
prompt=0

set -- `getopt npy $*`

while [ $1 != -- ]; do
	case $1 in
				-n)
					update=0
					;;
				-p)
					prompt=1
					;;
				-y)
					prompt=2
					;;
	esac
	shift
done
shift

cd $repo

if [ $update -eq 1 ]; then
	git remote update && git pull
fi

echo "Updating dotfiles..."

while read file; do
	src=`basename $file`
	dst="$HOME/$file";
	if [ -f $src -a -f $dst ]; then
		echo "$src -> $dst"
		cp $src $dst
	else
		echo "($dst not found)"
		if [ $prompt -gt 0 ]
		then
			if [ $prompt -eq 1 ]
			then
				read -p "Create? " create < /dev/tty
				echo
			elif [ $prompt -eq 2 ]
			then
				create="y"
			fi
			if [ $create = "y" ]; then
				dir=`dirname $file`
				if [ ! -d $HOME/$dir ]; then
					echo "creating directory $HOME/$dir"
					mkdir -p $HOME/$dir
				fi
				cp $src $dst
				echo "$src -> $dst (created)"
			fi
		fi
	fi
done < manifest;
