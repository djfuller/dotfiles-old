#!/bin/zsh

srcdir=$(dirname ${(%):-%N})
set -x

for i in .exrc .vimrc .zshrc .screenrc
do
	cp $srcdir/$i $HOME
done

mkdir -p $HOME/vim/after
cp $srcdir/syncolor.vim $HOME/vim/after
cp $srcdir/tex.vim $HOME/vim/after
cp $srcdir/python.vim $HOME/vim/after

if [[ -n "${commands[tcsh]}" ]]
then
	cp $srcdir/.tcshrc $HOME
fi

if [[ -n "${commands[emacs]}" ]]
then
	cp $srcdir/.emacs $HOME
fi

if [[ -n "${commands[atom]}" ]]
then
	mkdir -p $HOME/.atom
	cp $srcdir/keymap.cson $HOME/.atom
	cp $srcdir/init.coffee $HOME/.atom
fi

if [[ -n "${commands[irssi]}" ]]
then
	mkdir -p $HOME/.irssi
	cp $srcdir/config $HOME/.irssi
fi

if [[ -n "${commands[znc]}" ]]
then
	mkdir -p $HOME/.znc/configs
	cp $srcdir/znc.conf $HOME/.znc/configs
fi
