set ai
set ts=3
set sw=3
set sm
set report=3
set wildchar=^I
set nocindent
set shortmess+=I
set mousef
set wildmode=longest
set more
set hlsearch
set incsearch
set formatoptions+=ro
set backspace=""
set bg=dark

"Fixed is the only acceptable GUI font for programming.
set guifont=fixed
set guioptions-=T
set guioptions-=m

"Must...have...syntax...highlighting...
"These values work on "standard" xterms, screens (pretty well), and most
"other color terminals that do ANSI stuff right.
"Note: these contain literal escape characters that won't survive a
"transmission medium that isn't 8-bit clean.
"if has("terminfo")
"  set t_Co=16
"  exec "set t_AB=\<Esc>[%?%p1%{8}%<%t%p1%{40}%+%e%p1%{92}%+%;%dm"
"  exec "set t_AF=\<Esc>[%?%p1%{8}%<%t%p1%{30}%+%e%p1%{82}%+%;%dm"
"else
"  set t_Co=16
"  exec "set t_Sf=\<Esc>[3%dm"
"  exec "set t_Sb=\<Esc>[4%dm"
"endif

"Does :nohlsearch unless called twice within 1-2 seconds, see the backup
"mapping for ,rc_v.

if exists(":fu")
	fu! NohOrVisual()
		if !exists("g:rc_vtime") || localtime() - g:rc_vtime > 1
			let g:rc_vtime = localtime()
			return ":nohlsearch\<CR>\<C-L>"
		else
			return ",rc_v"
		endif
	endf

	fu! SmartTab()
		let col = col('.') - 1
		if !col || getline('.')[col - 1] !~ '\k'
			return "\<TAB>"
		else
			return "\<C-P>"
		endif
	endf

	inoremap <TAB> <C-R>=SmartTab()<CR>
	noremap ,rc_v v
	noremap v @=NohOrVisual()<CR>

endif

"We map something to 'v' here so it can be used in a function that essentially
"echoes those characters.
noremap <C-V> :exec b:ftcmd<CR><CR>
cabbrev wmake w \| make
cabbrev vim vi

au!

let b:ftcmd = "echo \"No filetype command defined.\""

"Change directories to the current directory of the buffer we're editing.
au BufEnter * if bufname("") !~ "^\[A-Za-z0-9\]*://" | sil! lcd %:p:h | endif
au BufEnter *.upc setf c
au BufEnter * setlocal nocindent

au FileType c,cpp let b:ftcmd = "split %<.h"
au FileType tex 
	\  if getline(search("^\\\\documentlcass.*","w")) =~ ".*a4paper.*"	|
	\		let $PAPERSIZE="a4"															|
	\	endif																					|
	\	if getline(search("^\\\\documentclass.*","w")) =~ ".*pdf.*"	|
	\		setlocal makeprg=pdflatex\ \%										|
	\		if has("unix") && system("uname") =~ "Darwin"				|
	\			let b:ftcmd = "!open %<.pdf"									|
	\		else																		|
	\			let b:ftcmd = "!xpdf %<.pdf &"								|
	\		endif																		|
	\	else																			|
	\		setlocal makeprg=latex\ \%\ &&\ dvips\ -o\ %<.ps\ \%<		|
	\		if has("unix") && system("uname") =~ "Darwin"				|
	\			let b:ftcmd = "!open %<.ps"									|
	\		else																		|
	\			let b:ftcmd = "!gv --watch %< &"								|
	\		endif																		|
	\	endif

filetype plugin on
syntax enable
