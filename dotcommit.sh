#!/bin/sh

repo="$HOME/dotfiles"

echo "Commiting dotfiles..."

cd $repo

while read file; do
	if [ -f $HOME/$file ]; then
		echo "$HOME/$file -> `basename $file`"
		cp $HOME/$file $repo
	else
		echo "($HOME/$file not found)"
	fi
done < manifest;

cd $repo
git commit -a
git push
